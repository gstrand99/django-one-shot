from django.urls import path
from .views import TodoListCreateView, todo_list_detail, todo_list

app_name = "todos"

urlpatterns = [
    path("", todo_list, name="todo_lists"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", TodoListCreateView.as_view(), name="todo_list_create"),
]
